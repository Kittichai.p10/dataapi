import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, Col, Row } from 'react-bootstrap';


export default class fromsignin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      remember: '',
      user: null,
      token: null,
      expires_in: null
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUerChange = this.handleUerChange.bind(this);
    this.handlePassChange = this.handlePassChange.bind(this);
    this.handleRememChange = this.handleRememChange.bind(this);
  }

  componentDidMount() {
    console.log('componentDidMount')
    let user = window.localStorage.getItem('user')
    console.log('user ::: ', user)
    if (user) {
      user = JSON.parse(user)
      this.setState({ user, token: user.token, expires_in: user.expires_in })
    }
  }


  handleUerChange = (e) => {
    this.setState({
      username: e.target.value
    });
  }

  handlePassChange = (e) => {
    this.setState({
      password: e.target.value
    })
  }

  handleRememChange = (e) => {
    this.setState({
      remember: 1
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const username = this.state.username
    const password = this.state.password
    const remember = this.state.remember

    const data = {
      username,
      password,
      remember
    }

    axios.post('https://substock.gethomestay.com/login/', data)
      .then((res) => {
        console.log("test res : ", res);
        console.log("test data : ", data);
        if (data.remember === 1) {
          window.localStorage.setItem('user', JSON.stringify({
            user: res.data.user,
            token: res.data.token,
            expires_in: res.data.expires_in
          }))
        }
        this.setState({
          user: res.data.user,
          token: res.data.token,
          expires_in: res.data.expires_in
        })
      }, (error) => {
        console.log(error);
      });
    console.log('UserData', data);
  }

  render() {
    return (
      <div>
        {!this.state.user ? <Form onSubmit={this.handleSubmit}>
          <Form.Group as={Row} controlId="username">
            <Form.Label column sm={2}>
              Username
                    </Form.Label>
            <Col sm={10}>
              <Form.Control type="text" value={this.state.username} onChange={this.handleUerChange} required />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="password">
            <Form.Label column sm={2}>
              Password
                    </Form.Label>
            <Col sm={10}>
              <Form.Control type="password" value={this.state.password} onChange={this.handlePassChange} required />
            </Col>
          </Form.Group>

          <Form.Group controlId="remember">
            <Form.Check type="checkbox" label="Remember" value="0" onChange={this.handleRememChange} required />
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form> : <ShowToken
          user={this.state.user}
          token={this.state.token}
          expires_in={this.state.expires_in}
          onLogout={() => {
            window.localStorage.removeItem('user')
            this.setState({
              user: null,
              token: null,
              expires_in: null
            })
          }}></ShowToken>}
      </div>
    )
  }
}

const ShowToken = (props) => {
  const { user, token, expires_in, onLogout } = props
  console.log('show token conpoment : ', user, token, expires_in)
  return <div>
          <h3>
            TOKEN : {token}
          </h3>
          <h3>
            expires_in : {expires_in}
          </h3>
          <Button onClick={() => { onLogout() }}>Logout</Button>
        </div>
}